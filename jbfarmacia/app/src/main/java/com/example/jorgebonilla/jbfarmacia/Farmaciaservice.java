package com.example.jorgebonilla.jbfarmacia;

import com.example.jorgebonilla.jbfarmacia.dto.FarmaciaDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface Farmaciaservice {
    @Headers({
            "Authorization: Basic YWRtaW46YWRtaW4="
    })
    @GET("/farmacias")
    Call<List<FarmaciaDTO>> getAllPharm();
    
}
