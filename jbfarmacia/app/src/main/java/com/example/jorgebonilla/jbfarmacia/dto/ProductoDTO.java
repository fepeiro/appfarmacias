/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jorgebonilla.jbfarmacia.dto;

import java.util.UUID;

/**
 *
 * @author Felipe
 */
public class ProductoDTO {

    private String strid, nombre;
    private float precio;

    public ProductoDTO() {
        this.strid = UUID.randomUUID().toString();
    }

    public ProductoDTO(String name) {
        this.strid = UUID.randomUUID().toString();
        this.nombre = name;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the strid
     */
    public String getStrid() {
        return strid;
    }

    /**
     * @param strid the strid to set
     */
    public void setStrid(String strid) {
        this.strid = strid;
    }

    /**
     * @return the precio
     */
    public float getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }
}
