/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jorgebonilla.jbfarmacia.dto;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author Felipe
 */
public class FarmaciaDTO {

    private Map<String, ProductoDTO> productos;
    private float latitud, longitud;
    private String strid, nombre, dueno;

    public FarmaciaDTO() {
        productos = new LinkedHashMap<>();
        strid = UUID.randomUUID().toString();
        nombre = "";
        dueno = "";
    }

    /**
     * @return the productos
     */
    public Map<String, ProductoDTO> getProductos() {
        return productos;
    }

    /**
     * @param productos the productos to set
     */
    public void setProductos(Map<String, ProductoDTO> productos) {
        this.productos = productos;
    }

    /**
     * @return the strid
     */
    public String getStrid() {
        return strid;
    }

    /**
     * @param strid the strid to set
     */
    public void setStrid(String strid) {
        this.strid = strid;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the latitud
     */
    public float getLatitud() {
        return latitud;
    }

    /**
     * @param latitud the latitud to set
     */
    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    /**
     * @return the longitud
     */
    public float getLongitud() {
        return longitud;
    }

    /**
     * @param longitud the longitud to set
     */
    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }

    /**
     * @return the dueno
     */
    public String getDueno() {
        return dueno;
    }

    /**
     * @param dueno the dueno to set
     */
    public void setDueno(String dueno) {
        this.dueno = dueno;
    }
}
